#!/bin/bash
tag=$([ -z $1 ] && echo "latest" || echo $1)
docker build -t cgrandsjo/cybercom-joke-generator:${tag} .
