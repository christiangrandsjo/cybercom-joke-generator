import React from "react";
import ReactDOM from "react-dom";
import JokerDetail from "./components/JokeDetail";

class App extends React.Component {
  onButtonClicked = event => {
    const jokerIndex = Math.floor(Math.random() * 3);
    this.setState({ currentJoker: this.jokers[jokerIndex] });
  };

  constructor(props) {
    super(props);
    this.state = {
      currentJoker: null
    };
  }

  jokers = [
    {
      name: "Niklas Flyborg",
      imageSrc: "/niklas.png"
    },
    {
      name: "Camilla Öberg",
      imageSrc: "/camilla.png"
    },
    {
      name: "Bo Strömqvist",
      imageSrc: "/bo.png"
    }
  ];

  render() {
    console.log("Joker:", this.state.currentJoker);

    return (
      <div className="ui container">
        <h2 style={{ marginTop: "20px" }}>Cybercom Joke Generator</h2>
        <button
          onClick={this.onButtonClicked}
          className="fluid ui massive primary button"
        >
          Make me laugh!
        </button>
        <div className="ui raised very padded text container segment">
          {this.state.currentJoker ? (
            <JokerDetail joker={this.state.currentJoker} />
          ) : (
            <> </>
          )}
        </div>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
