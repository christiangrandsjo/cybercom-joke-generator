import React from "react";
import icanhazdadjoke from "../api/icanhazdadjoke";

class JokerDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      joke: ""
    };
  };

  getJoke = async () => {
    const response = await icanhazdadjoke.get();
    this.setState({ joke: response.data.joke });
  };

  componentDidMount() {
    this.getJoke();
  };

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.getJoke();
    };
  };

  render() {
    return (
      <div className="ui centered card">
        <div className="ui slide masked reveal image">
          <img
            alt={this.props.joker.name}
            src={this.props.joker.imageSrc}
            className="visible content"
          ></img>
          <div className="hidden content">
            <p className="ui massive message">{this.state.joke}</p>
          </div>
        </div>
        <div className="content">
          <h2 className="ui header">{this.props.joker.name}</h2>
        </div>
      </div>
    );
  };
};

export default JokerDetail;
