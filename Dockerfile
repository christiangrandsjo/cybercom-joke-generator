FROM node:12-alpine

LABEL MAINTAINER="Secret <secret@mail.com>"
RUN mkdir /app
COPY package*.json /app/
WORKDIR /app

RUN apk --no-cache add --virtual native-deps \
    g++ gcc libgcc libstdc++ linux-headers make python && \
    npm install node-gyp -g && \
    npm install && \
    apk del native-deps && \
    npm cache clean --force

COPY . /app/
EXPOSE 3000
ENTRYPOINT ["npm", "run", "start"]
